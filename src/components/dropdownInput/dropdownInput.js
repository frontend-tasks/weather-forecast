import React, { useState, useEffect, useRef } from 'react';
import './dropdownInput.css';
import caretDown from './caret-down-solid.svg';
import caretUp from './caret-up-solid.svg';

function DropdownInput(props) {
    const [filteredItems, setFilteredItems] = useState([]);
    const [showDropdown, setShowDropdown] = useState(false);
    const [displayedItems, setDisplayedItem] = useState(50);
    const [dropdownIcon, setDropdownIcon] = useState(`url(${caretDown})`);
    const dropdownRef = useRef(null);

    useEffect(() => {
        setFilteredItems(props.items);
    }, [props.items]);

    useEffect(() => {
        const img = showDropdown ? caretUp : caretDown;
        setDropdownIcon(`url(${img})`);
    }, [showDropdown]);

    const handleInputChange = (event) => {
        const value = event.target.value.toLowerCase();

        const filtered = props.items.filter(item => {
            return item.city.toLowerCase().includes(value) || item.zip_code.toString().includes(value);
        });

        setFilteredItems(filtered);
    }

    const handleInputClick = () => {
        setShowDropdown(true);
        setFilteredItems(props.items);
    }

    const handleScroll = () => {
        const dropdown = dropdownRef.current;
        if (dropdown && dropdown.scrollTop + dropdown.clientHeight === dropdown.scrollHeight) {
            setDisplayedItem(displayedItems + 50);
        }
    }

    const handleKeyPress = (event) => {
        if (event.key === "Enter") {
            const inputValue = event.target.value.trim().toLowerCase();
            if (inputValue !== '') {
                // Check if the selected zip code exists in the list
                setShowDropdown(false);
                // Clear the input field and remove focus
                event.target.value = '';
                event.target.blur();
                props.setZipCodeList(filteredItems);
            }
        }
    }

    const handleItemClick = (item) => {
        setShowDropdown(false);
        props.setZipCodeList([item]);
    }

    return (
        <div className="dropdown">
            <div className="dropdown-input">
                <input className="input-field" type="text" id="input" placeholder={props.placeholder} onChange={handleInputChange} onKeyPress={handleKeyPress} onClick={handleInputClick} style={{ backgroundImage: dropdownIcon }} />
            </div>
            {showDropdown && (
                <ul className="dropdown-content" ref={dropdownRef} onScroll={handleScroll}>
                    {filteredItems.slice(0, displayedItems).map((item) => (
                        <li key={item.zip_code} onClick={() => handleItemClick(item)}>{item.zip_code} ({item.city})</li>
                    ))}
                </ul>
            )}
        </div>
    );
}

export default DropdownInput;