import './weatherModal.css';
import React, { useState } from 'react';
import { BiChevronDown, BiChevronUp } from 'react-icons/bi';
import { getDay } from '../../utils/utils.js';

function WeatherModal({ weather, handleClose }) {
    const [unit, setUnit] = useState('celsius');
    const [showForecast, setshowForecast] = useState(false);
    const forecastData = weather.forecast.forecastday;

    const handleUnitToggle = () => {
        setUnit(prevUnit => prevUnit === 'celsius' ? 'fahrenheit' : 'celsius');
    }

    const getTemperatureValue = (temp) => {
        if (unit === 'celsius') {
            return temp.celsius;
        } else {
            return temp.fahrenheit;
        }
    }

    return (
        <div className="modal">
            <span className="close" onClick={handleClose}>
                &times;
            </span>
            <div className="modal-content">
                <div className="row-1">
                    <div className="left-content">
                        <img
                            src={weather.current.condition.icon}
                            alt={weather.current.condition.text}
                        />
                        <div className="temperature">
                            {getTemperatureValue({ celsius: weather.current.temp_c, fahrenheit: weather.current.temp_f })}
                            <sup className="temp-unit">
                                <button onClick={handleUnitToggle}>
                                    <span style={{ fontWeight: unit === 'celsius' ? 'bold' : 'normal' }}>°C</span> |
                                    <span style={{ fontWeight: unit === 'fahrenheit' ? 'bold' : 'normal' }}>°F</span>
                                </button>
                            </sup>
                        </div>
                        {showForecast && (<div className="weather-info">
                            <p>Precipitation: {weather.current.precip_in}</p>
                            <p>Humidity: {weather.current.humidity}</p>
                            <p>Wind: {weather.current.wind_kph} km/h</p>
                        </div>)}
                    </div>
                    <div className="right-content">
                        <div className="column">
                            <h3>{weather.location.name}, {weather.location.region}</h3>
                            <p className="date">{weather.current.last_updated.split(" ")[0]}</p>
                            <p className="condition">{weather.current.condition.text}</p>
                        </div>
                        <div className="column" onClick={() => setshowForecast(!showForecast)}>
                            <span className="weather-downdown-icon"> {showForecast ? <BiChevronUp /> : <BiChevronDown />}</span>
                        </div>
                    </div>
                </div>
                {showForecast && (
                    <div className="row-2">
                        <div className="forecast">
                            {forecastData.map((forecast, index) => (
                                <article className="forecast-card" key={index}>
                                    <h3>{getDay(forecast.date)}</h3>
                                    <img
                                        src={forecast.day.condition.icon}
                                        alt={forecast.day.condition.text}
                                    />
                                    <p className="condition">{forecast.day.condition.text}</p>
                                    <p> {getTemperatureValue({
                                        celsius: forecast.day.mintemp_c,
                                        fahrenheit: forecast.day.mintemp_f
                                    })}
                                        {'° - '}
                                        {getTemperatureValue({
                                            celsius: forecast.day.maxtemp_c,
                                            fahrenheit: forecast.day.maxtemp_f
                                        })}°
                                    </p>
                                </article>
                            ))}
                        </div>
                    </div>)}
            </div>
        </div >
    );
}

export default WeatherModal;
