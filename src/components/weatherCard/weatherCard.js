import './weatherCard.css';

function WeatherCard({ weather, onCardClick }) {
    return (
        <div className="card-container">
            {weather.map((weatherData, index) => (
                <div className="card" key={index} onClick={() => onCardClick(weatherData)}>
                    <h2 className="card-heading">{weatherData.location.name}</h2>
                    <div className="card-content">
                        <p className="card-info">Region: {weatherData.location.region}</p>
                        <p className="card-info">Coordinate: {weatherData.location.lat} {weatherData.location.lon}</p>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default WeatherCard;
