import React, { useState, useEffect } from 'react';
import { ZIP_CODE_URL, WEATHER_API_BASE_URL } from './constant/index.js'
import DropdownInput from './components/dropdownInput/dropdownInput.js';
import WeatherCard from './components/weatherCard/weatherCard.js';
import WeatherModal from './components/weatherModal/weatherModal.js';
import './App.css';


function App() {
  const [cities, setCities] = useState([]);
  const [zipCodeList, setZipCodeList] = useState([]);
  const [weatherList, setWeatherList] = useState([]);
  const [showCardsGrid, setShowCardsGrid] = useState(false);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    fetch(ZIP_CODE_URL)
      .then(response => response.json())
      .then(data => {
        const sortedItems = data.sort((a, b) => {
          if (!a.zip_code || !b.zip_code) {
            return 0;
          }
          return a.zip_code.toString().localeCompare(b.zip_code);
        });
        setCities(sortedItems);
      });
  }, []);

  useEffect(() => {
    const fetchWeatherData = async () => {
      const promises = zipCodeList.map((item) => {
        return fetch(WEATHER_API_BASE_URL + `?key=${process.env.REACT_APP_API_KEY}&q=${item.zip_code}&days=3`)
          .then(response => response.json())
          .then(data => data)
          .catch(error => {
            console.error('Error fetching weather data:', error);
            return null;
          });
      });
      const data = await Promise.all(promises);
      setWeatherList(data.filter(item => item !== null && !item.error));
      setShowCardsGrid(true);
    };

    const intervalId = setInterval(() => {
      fetchWeatherData();
    }, 30000);

    fetchWeatherData();

    return () => clearInterval(intervalId);

  }, [zipCodeList]);

  const handleClose = () => {
    setShowModal(false);
  }

  return (
    <div>
      <div className="center">
        <h1 className="app-heading">Weather Forecast</h1>
        <DropdownInput placeholder="Zip Code" items={cities} setZipCodeList={setZipCodeList} />
        {showCardsGrid && weatherList && (
          <WeatherCard weather={weatherList} onCardClick={setShowModal} />
        )}
        {showModal && (<WeatherModal weather={showModal} handleClose={handleClose} />)}
      </div>
    </div>
  );
}

export default App;