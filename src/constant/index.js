export const ZIP_CODE_URL = 'https://raw.githubusercontent.com/millbj92/US-Zip-Codes-JSON/master/USCities.json';
export const WEATHER_API_BASE_URL = 'https://api.weatherapi.com/v1/forecast.json';